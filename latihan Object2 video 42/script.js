// Membuat Object
// Object Literal
var mhs1 = {
    nama : 'sri yustinia',
    nrp : '0403040023',
    jurusan :'rekayasa perangkat lunak'
}


var mhs2 = {
    nama : 'doddy',
    nrp : '0403040023',
    jurusan :'teknik industri'
}


// Function Declaration
function buatObjectMahasiswa(nama, nrp, jurusan){
    var mhs = {};
    mhs.nama = nama;
    mhs.nrp = nrp;
    mhs.jurusan = jurusan;
    return mhs ;
}


var mhs3 = buatObjectMahasiswa('Nofariza', '023040123',  'teknik pangan');



// Constructor
function Mahasiswa(nama, nrp, jurusan) {
    // var this = {};
    this.nama = nama;
    this.nrp = nrp;
    this.jurusan = jurusan;
    // return this;
}

var mhs4 = new Mahasiswa('Erik', '01020304', 'teknik mesin'); 